﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController2 : MonoBehaviour {

	//create enum for AttackMode choice
	public enum AttackMode {Chase, Flee};
	public AttackMode attackMode;
	//create Transform variable for target
	public Transform target;
	//create variable for transform to increase framerate
	public Transform tf;

	//create variable for distance for enemy tank to flee
	public float fleeDistance = 1.0f;

	//access TankData script
	private TankData data;
	//access TankMotor script
	private TankMotor motor;

	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();
		motor = GetComponent<TankMotor> ();
	}
	
	// Update is called once per frame
	void Update () {

		//if attackMode chosen is Chase
		if (attackMode == AttackMode.Chase) {
			//rotate towards target
			motor.RotateTowards (target.position, data.turnSpeed);
			//and move towards target
			motor.Move (data.moveSpeed);
		}

		//if attackMode chosen is Flee
		if (attackMode == AttackMode.Flee) {
			//find vector from position to target
			Vector3 vectorToTarget = target.position - tf.position;
			//flip vector to opposite direction to get away from target
			Vector3 vectorAwayFromTarget = -vectorToTarget;
			//normalize vector to give it a magnitude of 1
			vectorAwayFromTarget.Normalize ();

			//normalized vector can be multiplied by a length to make
			// a vector of that length
			vectorAwayFromTarget *= fleeDistance;

			//find new position to flee to
			Vector3 fleePosition = vectorAwayFromTarget + tf.position;
			//rotate and move there
			motor.RotateTowards (fleePosition, data.turnSpeed);
			motor.Move (data.moveSpeed);


		}
	}
}
