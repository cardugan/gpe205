﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour {

	//create List to store dynamic group of data
	//List <type of data> name of list
	public List <Powerup> powerups;

	//create variable to hold TankData script
	private TankData data;

	// Use this for initialization
	void Start () {
		//initialize List
		powerups = new List <Powerup> ();
		//get TankData component for this gameObject
		data = gameObject.GetComponent <TankData> ();
	}
	
	// Update is called once per frame
	void Update () {
		//iterate through powerups list to check for expired ones
		//create new list of expired powerups
		List <Powerup> expiredPowerups = new List <Powerup> ();
		//loop through first list
		foreach (Powerup power in powerups) {
			//subtract from the timer
			power.duration -= Time.deltaTime;

			//add expired powers to new list
			if (power.duration <= 0) {
				expiredPowerups.Add (power);
			}
		}

		//deactivate expired powers on list
		foreach (Powerup power in expiredPowerups) {
			power.OnDeactivate (data);
			//and remove from first powerups list
			powerups.Remove (power);
		}
		//expiredPowerups list is local and will disappear, but...
		expiredPowerups.Clear ();
	}

	public void Add (Powerup powerup) {
		//activate powerup on gameObect data is attached to
		powerup.OnActivate (data);
		//if not a permanent powerup
		if (!powerup.isPermanent) {
			//add powerup to list
			powerups.Add (powerup);
		}
	}


}
