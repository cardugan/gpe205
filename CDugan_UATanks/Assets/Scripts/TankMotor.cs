﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankMotor : MonoBehaviour {
	//create variable for CharacterController component
	private CharacterController characterController;
	//create variable for TankData 
	public TankData data;
	//create variable for transform to increase framerate
	private Transform tf;


	public void Awake() {
		tf = gameObject.GetComponent<Transform> ();
	}

	// Use this for initialization
	void Start () {
		//store CharacterController in variable to get access
		characterController = gameObject.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Move (float speed) {
	//function to move tank forward
		//create vector to hold speed data
		Vector3 speedVector;
		//point speedVector in same direction as tank
		speedVector = tf.forward;
		//apply speed value
		speedVector *= speed;
		//call SimpleMove() and send it to speedVector, converts to m/s
		characterController.SimpleMove (speedVector);
	}

	public void Rotate (float speed) {
		//function to rotate tank around y-axis (0,1,0)
		//create vector to hold rotation data
		Vector3 rotateVector;

		//right is positive, left is negative
		rotateVector = Vector3.up;
		//adjust based on speed
		rotateVector *= speed;
		//convert to unit per second
		rotateVector *= Time.deltaTime;

		//do the rotation in local space
		tf.Rotate (rotateVector, Space.Self);

	}

	public bool RotateTowards (Vector3 target, float speed) {
	//function to rotate towards target if possibe
	//return true if able to rotate, return false if already rotated towards target

		//difference between current position and target
		Vector3 vectorToTarget = target - tf.position;

		//find rotation that looks down the vector
		Quaternion targetRotation = Quaternion.LookRotation (vectorToTarget);
		//if already turned that direction
		if (targetRotation == tf.rotation) {
			//do not need to turn
			return false;
		}

		//otherwise make the rotation
		tf.rotation = Quaternion.RotateTowards (tf.rotation, targetRotation, data.turnSpeed * Time.deltaTime);

		//we rotated so... 
		return true;
	}
}
