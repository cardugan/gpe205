﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {

	//create enum for input schemes
	public enum InputScheme { WASD, arrowKeys };
	//create variable for input schemes
	public InputScheme input = InputScheme.arrowKeys;
	//create variable for access to TankMotor script
	public TankMotor motor;
	//create variable for access to TankData script
	public TankData data;
	//create variable for access to Shooter script
	public Shooter shoot;

	//create timer to track time before next allowed shot
	private float delayTimer = 0;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		//shooting controls for tank cannon
		//start timer 
		delayTimer -= Time.deltaTime;

		if (Input.GetKey (KeyCode.Space) && delayTimer <= 0) {
			Debug.Log ("Bang!");
			shoot.ShootCannon (data.shotForce, data.shellDestruct);
			//set delay on timer
			delayTimer = data.fireDelay;
		}

		//set up switch to handle user inputs for tank movement
		switch (input) {
		case InputScheme.arrowKeys:
			if (Input.GetKey (KeyCode.UpArrow)) {
				motor.Move (data.moveSpeed);
			}
			if (Input.GetKey (KeyCode.DownArrow)) {
				motor.Move (-data.moveSpeed);
			}
			if (Input.GetKey (KeyCode.RightArrow)) {
				motor.Rotate (data.turnSpeed);
			}
			if (Input.GetKey (KeyCode.LeftArrow)) {
				motor.Rotate (-data.turnSpeed);
			}
			break;
		case InputScheme.WASD:
			if (Input.GetKey (KeyCode.W)) {
				motor.Move (data.moveSpeed);
			}
			if (Input.GetKey (KeyCode.S)) {
				motor.Move (-data.moveSpeed);
			}
			if (Input.GetKey (KeyCode.D)) {
				motor.Rotate (data.turnSpeed);
			}
			if (Input.GetKey (KeyCode.A)) {
				motor.Rotate (-data.turnSpeed);
			}
			break;
		}
	}
}
