﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Powerup  {

	//variables to hold powerups
	public float speedModifier;
	public float healthModifier;
	public float maxHealthModifier;
	public float fireRateModifier;

	//variable to hold how long powerup lasts
	public float duration;
	//bool for permanent powerups
	public bool isPermanent;

	public void OnActivate (TankData target) {
		//assign modifications that will start on acivation
		target.moveSpeed += speedModifier;
		target.health += healthModifier;
		target.maxHealth += maxHealthModifier;
		target.fireDelay -= fireRateModifier;
	}

	public void OnDeactivate (TankData target) {
		//assign modifications to be reversed on deactivation
		target.moveSpeed -= speedModifier;
		target.health -= healthModifier;
		target.maxHealth -= maxHealthModifier;
		target.fireDelay += fireRateModifier;
	}
}
