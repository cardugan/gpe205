﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITimid : MonoBehaviour {

	//create enum to choose AI state
	public enum AIState {LookFor, Stop, CheckForFlee, Flee};
	public AIState aiState = AIState.LookFor;
	//create Transform variable for target
	public Transform target;
	//variable to hold time since current state was entered
	public float stateEnterTime;
	//variable to hold radius around AI to "hear" player
	public float aiSenseRadius;

	//access TankMotor script
	private TankMotor motor;
	//access TankData script
	private TankData data;

	//create variable for transform to increase framerate
	private Transform tf;

	//variable to hold which step of avoidance algorithm we are in
	//0 is normal movement state
	private int avoidanceStage = 0;
	//how long AI stays in this state (adjustable)
	public float avoidanceTime = 2.0f;
	//variable to track how long until AI exits state
	private float exitTime;
	//create variable for distance for AI to flee
	public float fleeDistance = 5.0f;

	//vision sense of AI: variable to hold field of vision
	public float fieldOfView = 45.0f;
	//variable to hold vision distance
	public float viewDistance = 10.0f;
	//variable to hold gameObject to look for
	public GameObject player;

	public void Awake () {
		tf = gameObject.GetComponent<Transform> ();
	}

	// Use this for initialization
	void Start () {
		data = GetComponent <TankData> ();
		motor = GetComponent <TankMotor> ();
	}
	
	// Update is called once per frame
	void Update () {
		 
		//AIState LookFor
		if (aiState == AIState.LookFor) {
			//perform behaviors
			//turn towards target (source of "noise") to look for
			motor.RotateTowards (target.position, data.turnSpeed);

			//check for transitions
			if (CanSee (player)) {
				ChangeState (AIState.Stop);
			}
			//if not in FOV or blocked by obstacle, return to LookFor
			else {
				ChangeState (AIState.CheckForFlee);
			}
		} //end AIState.LookFor

		//AIState.CheckForFlee
		else if (aiState == AIState.CheckForFlee) {
			//perform behaviors
			CheckForFlee ();

			//check for transitions
			//if player is too close, flee
			if (Vector3.SqrMagnitude (target.position - tf.position) <= (aiSenseRadius * aiSenseRadius)) {
				ChangeState (AIState.Flee);
				//if player is far enough away, go back to looking for player
			} else {
				ChangeState (AIState.LookFor);
			}

		} //end AIState.CheckForFlee

		//AIState.Flee
		else if (aiState == AIState.Flee) {
			//perform behaviors
			if (avoidanceStage != 0) {
				DoAvoidance ();
			} else {
				DoFlee ();
			}

			//check for transitions
			//if in AIState.Flee for long enough, change to .CheckForFlee
			if (Time.time >= stateEnterTime + 30) {
				ChangeState (AIState.CheckForFlee);
			}

		} //end AIState.Flee

		//AIState.Stop
		else if (aiState == AIState.Stop) {
			//perform behaviors
			Stop ();

			//check for transitions
			if (Time.time >= stateEnterTime + 1.5f) {
				ChangeState (AIState.CheckForFlee);
			}

		} //end AIState.Stop

	} //end Update ()

	public void CheckForFlee () {
		//TODO: Write function
	}

	public void Stop () {
		//TODO: Write function
	}


	public void DoFlee () {
		//find vector from position to target
		Vector3 vectorToTarget = target.position - tf.position;
		//flip vector to opposite direction to get away from target
		Vector3 vectorAwayFromTarget = -vectorToTarget;
		//normalize vector to give it a magnitude of 1
		vectorAwayFromTarget.Normalize ();

		//normalized vector can be multiplied by a length to make
		// a vector of that length
		vectorAwayFromTarget *= fleeDistance;

		//find new position to flee to
		Vector3 fleePosition = vectorAwayFromTarget + tf.position;
		//rotate and move there
		motor.RotateTowards (fleePosition, data.turnSpeed);
		if (CanMove (data.moveSpeed)) {
			motor.Move (data.moveSpeed);
		} else {
			//enter avoidance stage 1
			avoidanceStage = 1;
		}	
	}
		

	public void ChangeState (AIState newState) {
		//change state
		aiState = newState;

		//save the time we changed states
		stateEnterTime = Time.time;
	}

	public void DoAvoidance () {
		//handles obstacle avoidance
		if (avoidanceStage == 1) {
			//rotate left
			motor.Rotate (-data.turnSpeed);
			//if able to move forward, move to stage 2
			if (CanMove (data.moveSpeed)) {
				avoidanceStage = 2;
				//set number of seconds we will stay in stage 2
				exitTime = avoidanceTime;
			}
			//otherwise we'll do this again next turn
		}else if (avoidanceStage == 2) {
			//move forward if able
			if (CanMove (data.moveSpeed)) {
				//subtract from timer and move
				exitTime -= Time.deltaTime;
				motor.Move (data.moveSpeed);
				//if timer runs out, return to normal mode
				if (exitTime <= 0) {
					avoidanceStage = 0;
				}
			} else {
				//if can't move forward, go back to stage 1
				avoidanceStage = 1;
			}
		}

	}

	bool CanMove (float speed) {
		//cast ray forward to see if it hits anything
		RaycastHit hit;
		//if it hits something
		if (Physics.Raycast (tf.position, tf.forward, out hit, speed)) {
			//and if what we hit is not the player
			if (!hit.collider.CompareTag ("Player")) {
				//then we can't move
				return false;
			}
		}
		//otherwise we can move
		return true;
	}

	public bool CanSee (GameObject target) {

		//store location of target in variable
		Vector3 targetPosition = target.transform.position;

		//find vector form AI to player
		Vector3 aiToTargetVector = targetPosition - tf.position;

		//find the angle between the direction our agent is facing and the vector to the target
		float angleToTarget = Vector3.Angle (aiToTargetVector, tf.forward);

		//if the angle is less than our FOV 
		if (angleToTarget < fieldOfView) {
			//create variable to how ray from ai to target
			Ray rayToTarget = new Ray ();
			//set origin and direction of ray
			rayToTarget.origin = tf.position;
			rayToTarget.direction = aiToTargetVector;

			//create variable to hold info about what ray hits
			RaycastHit hit;

			//cast ray and see if it hits something
			if (Physics.Raycast (rayToTarget, out hit, viewDistance)) {
				//if it hits the target
				if (hit.collider.gameObject == target) {
					//canSee is true
					return true;
				}
			}
		}
		//canSee is false
		return false;
	}

}
