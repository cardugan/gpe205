﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour {

	//variable to hold powerup data
	public Powerup powerup;
	//sound to let player know pickup was picked up
	public AudioClip feedback;


	//create variable for transform to increase framerate
	private Transform tf;




	// Use this for initialization
	void Start () {
		tf = gameObject.GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
	}



	public void OnTriggerEnter (Collider other) {
		//variable to store other object's PowerupController, if it has one
		PowerupController powCon = other.GetComponent <PowerupController> ();

		//if other object does have a PowerupController
		if (powCon != null) {
			//add the powerup
			powCon.Add (powerup);

			//play feedback (if it is set)
			if (feedback != null) {
				AudioSource.PlayClipAtPoint (feedback, tf.position, 1.0f);
			}

			//destroy this pickup
			Destroy (gameObject);

		}
	}
}
