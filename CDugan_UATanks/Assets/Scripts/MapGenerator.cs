﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour {

	//create variables to hold number of rows and columns in map
	public int rows;
	public int cols;
	//create variables for tile template dimensions
	private float roomWidth = 50.0f;
	private float roomHeight = 50.0f;

	//create array to hold different tiles
	public GameObject[] gridPrefabs;

	//create array to store grid after it's generated
	private Room [,] grid;

	//create variable to allow random seed (editable)
	public int mapSeed;

	//create bool for if map-of-the-day wanted
	public bool isMapOfTheDay;
	//create bool for if random map wanted
	public bool isRandomMap;


	//function to return random room/tile
	public GameObject RandomRoomPrefab () {
		return gridPrefabs [UnityEngine.Random.Range (0, gridPrefabs.Length)];
	}

	//function to return current date/time object as an int
	public int DateToInt (DateTime dateToUse) {
		//add the date up and return the result
		return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour +
		dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
	}

	public void GenerateGrid () {
		//set random seed
		UnityEngine.Random.InitState (mapSeed);

		//clear grid - "which column" is our X, "which row" is our Y
		grid = new Room[cols,rows];

		//for each grid row
		for (int i = 0; i < rows; i++) {
			//for each column in that row
			for (int j = 0; j < cols; j++) {
				//figure out the location
				float xPosition = roomWidth * j;
				float zPosition = roomHeight * i;
				Vector3 newPosition = new Vector3 (xPosition, 0.0f, zPosition);
			
				//create a new grid at the appropriate location
				GameObject tempRoomObj = Instantiate (RandomRoomPrefab(), newPosition, 
					Quaternion.identity) as GameObject;

				//set its parent
				tempRoomObj.transform.parent = this.transform;

				//give it a meaningful name
				tempRoomObj.name = "Room " +j+ "," +i;

				//get the room object
				Room tempRoom = tempRoomObj.GetComponent<Room> ();

				//open the inner doors
				//if in the bottom row, open the north door
				if (i == 0) {
					tempRoom.doorNorth.SetActive (false);
				} //if top row, open the south door
				else if (i == rows - 1) {
					tempRoom.doorSouth.SetActive (false);
				} //otherwise we are in the middle, open both doors
				else {
					tempRoom.doorNorth.SetActive (false);
					tempRoom.doorSouth.SetActive (false);
				}
				//if we are in the first column, open the east door
				if (j == 0) {
					tempRoom.doorEast.SetActive (false);
				} //if in last column, open west door
				else if (j == cols - 1) {
					tempRoom.doorWest.SetActive (false);
				} //otherwise we are in the middle, open both doors
				else {
					tempRoom.doorEast.SetActive (false);
					tempRoom.doorWest.SetActive (false);
				}
			
				//save it to the grid array
				grid [j, i] = tempRoom;
			}
		}
	}
	// Use this for initialization
	void Start () {
		//choose map of the day or random if wanted
		if (isMapOfTheDay) {
			mapSeed = DateToInt (DateTime.Now.Date);
		} else if (isRandomMap) {
			mapSeed = DateToInt (DateTime.Now);
		}
		
		//generate grid
		GenerateGrid ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
