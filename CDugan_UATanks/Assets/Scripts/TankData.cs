﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour {
	//variables whose values can be changed in the inspector
	public float moveSpeed = 3f; //in meters per second
	public float turnSpeed = 90f; //in degrees per second
	public float shotForce = 1000f; //force of projectile
	public float fireDelay = 0.5f; //delay between shots fired
	public float shellDestruct = 3f; //time until shells self-destruct

	public float maxHealth = 100f; //maximum health points
	public float health; //current health points



	// Use this for initialization
	void Start () {
		health = maxHealth;
	}

	// Update is called once per frame
	void Update () {
		
	}
}
