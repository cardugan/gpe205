﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController3 : MonoBehaviour {

	//create Transform variable for target
	public Transform target;
	//create variable for transform to increase framerate
	private Transform tf;
	//access TankData script
	private TankData data;
	//access TankMotor script
	private TankMotor motor;

	//variable to hold which step of avoidance algorithm we are in
	//0 is normal movement state
	private int avoidanceStage = 0;
	//how long AI stays in this state (adjustable)
	public float avoidanceTime = 2.0f;
	//variable to track how long until AI exits state
	private float exitTime;

	//create enum for AttackMode choice
	public enum AttackMode {Chase, Flee};
	public AttackMode attackMode;

	//runs before Start ()
	public void Awake () {
		tf = gameObject.GetComponent<Transform> ();
	}

	// Use this for initialization
	void Start () {
		data = GetComponent<TankData> ();
		motor = GetComponent<TankMotor> ();

	}
	
	// Update is called once per frame
	void Update () {
		//when attackMode is Chase
		if (attackMode == AttackMode.Chase) {
			if (avoidanceStage != 0) {
				DoAvoidance ();
			}
				else {
					DoChase ();
				}
		}
	}
				
	void DoChase () {
		//turn towards target to chase
		motor.RotateTowards (target.position, data.turnSpeed);
		//check if we can move "data.moveSpeed" units away 
		//this would be how far we could move in one second
		if (CanMove (data.moveSpeed)) {
			motor.Move (data.moveSpeed);
		} else {
			//enter avoidance stage 1
			avoidanceStage = 1;
		}
	}
				
	void DoAvoidance () {
		//handles obstacle avoidance
		if (avoidanceStage == 1) {
			//rotate left
			motor.Rotate (-data.turnSpeed);
			//if able to move forward, move to stage 2
			if (CanMove (data.moveSpeed)) {
				avoidanceStage = 2;
				//set number of seconds we will stay in stage 2
				exitTime = avoidanceTime;
			}
			//otherwise we'll do this again next turn
		}else if (avoidanceStage == 2) {
			//move forward if able
			if (CanMove (data.moveSpeed)) {
				//subtract from timer and move
				exitTime -= Time.deltaTime;
				motor.Move (data.moveSpeed);
				//if timer runs out, return to normal mode
				if (exitTime <= 0) {
					avoidanceStage = 0;
				}
			} else {
				//if can't move forward, go back to stage 1
				avoidanceStage = 1;
			}
		}

	}

	bool CanMove (float speed) {
		//cast ray forward to see if it hits anything
		RaycastHit hit;
		//if it hits something
		if (Physics.Raycast (tf.position, tf.forward, out hit, speed)) {
			//and if what we hit is not the player
			if (!hit.collider.CompareTag ("Player")) {
				//then we can't move
				return false;
			}
		}
		//otherwise we can move
		return true;
	}
}
