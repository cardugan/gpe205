﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour {

	//create variable for pickup prefab 
	public GameObject pickupPrefab;
	//create variable for time between spawns
	public float spawnDelay;
	//creat variable for next spawn time
	private float nextSpawnTime;
	//create variable for transform to increase framerate
	private Transform tf;
	//create variable for instatiated pickup
	GameObject spawnedPickup;

	// Use this for initialization
	void Start () {
		tf = gameObject.GetComponent<Transform> ();
		nextSpawnTime = Time.time + spawnDelay;
	}
	
	// Update is called once per frame
	void Update () {
		SpawnPickups ();
	}

	public void SpawnPickups () {

		//if no spawned pickup is there
		if (spawnedPickup == null) {
			//and if time to spawn pickup
			if (Time.time > nextSpawnTime) {
				//spawn and reset timer
				spawnedPickup = Instantiate (pickupPrefab, tf.position, Quaternion.identity) as GameObject;
				nextSpawnTime = Time.time + spawnDelay;
			}
		} else {
			//the spawned pickup is still there, just reset timer
			nextSpawnTime = Time.time + spawnDelay;
		}
	}
}
