﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleAIController : MonoBehaviour {

	//create enum for choice of pathways for enemy tank
	public enum LoopType {Stop, Loop, PingPong};
	public LoopType loopType;
	//bool to track direction for PingPong LoopType
	private bool isPatrolForward = true;
	
	//create array of transform variables for enemy tank waypoints
	public Transform [] waypoints;
	//access TankMotor script
	public TankMotor motor;
	//access TankData script
	public TankData data;
	//create variable for transform to increase framerate
	private Transform tf;

	//create variable to hold current waypoint
	private int currentWaypoint = 0;
	//create variable for distance from waypoint to call close enough
	public float closeEnough = 1.0f;

	public void Awake () {
		tf = gameObject.GetComponent<Transform> ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (motor.RotateTowards (waypoints [currentWaypoint].position, data.turnSpeed)) {
			//Do nothing!!
		} else {
			motor.Move (data.moveSpeed);
			//move forward
		}//end if/else
		//if close to waypoint, cycle to next waypoint in array or start over
		if (Vector3.SqrMagnitude (waypoints[currentWaypoint].position - tf.position) < 
			(closeEnough * closeEnough)) {
			if (loopType == LoopType.Stop) {
				//go through waypoints once
				if (currentWaypoint < waypoints.Length - 1) {
					currentWaypoint++;
				} //end if LoopType.Stop

			} else if (loopType == LoopType.Loop) {
				//go through waypoints once
				if (currentWaypoint < waypoints.Length - 1) {
					currentWaypoint++;
					//then start over
				} else {
					currentWaypoint = 0;
				}//end if/else
			}//end else if LoopType.Loop
			} else if (loopType == LoopType.PingPong) {
				if (isPatrolForward) {
					//go through waypoints once
					if (currentWaypoint < waypoints.Length - 1) {
						currentWaypoint++;
						//then turn around and go back
					} else {
						isPatrolForward = false;
						currentWaypoint--;
					}//end if/else
				} else if (isPatrolForward == false) {
					//isPatrolForward = false and tank is decrementing through waypoints
					if (currentWaypoint > 0) {
						currentWaypoint--;
					} else {
						//turn around and increment through waypoints
						isPatrolForward = true;
						currentWaypoint++;
					}//end if/else
				}//end else

		}//end LoopType.PingPong
		}//end Update()
}//end class
