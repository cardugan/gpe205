﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {
	
	//create variable for prefab shells
	public GameObject cannonShell;

	//create variable for TankData 
	public TankData data;
	//variable for point to fire shells from
	public Transform firePoint;



	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShootCannon (float force, float destruct) {
		
		//create shell and store it in a variable
		GameObject shell;

		//instantiate shell 1 unit in forward direction
		shell = Instantiate (cannonShell, firePoint.position, Quaternion.identity) as GameObject;

		//create new Shell object variable called shellData
		Shell shellData;
		//get Shell component from object we just instantiated
		shellData = shell.GetComponent<Shell> ();
		//set the shooter variable in that shellData to the data from the Shooter object
		shellData.shooter = data;

		//access rigidbody component of shell
		Rigidbody tempRigidbody;
		tempRigidbody = shell.GetComponent<Rigidbody> ();

		//add force vector to shell
		tempRigidbody.AddForce (force * firePoint.forward);

		//have shells self-destruct after time
		Destroy (shell, destruct);
	}


}
