﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour {
	
	//create variable for singleton instance
	public static GameManager instance;
	//access to TankData
	public TankData data;
	//array to hold enemy tank info
	public GameObject[] enemyTanks;
	//array to hold spawnpoints
	public Transform[] enemySpawnPoints;
	//list of index of spawnpoints
	List <int> SpawnIndex;

	public GameObject playerPrefab;


	//create enum for choice of map generation
	public enum MapType {RandomLevel, MapOfTheDay, PresetSeed};
	public MapType map = MapType.RandomLevel;
	//create variable to allow random seed (editable)
	public int mapSeed;
	//create variables to hold number of rows and columns in map
	public int rows;
	public int cols;
	//create variables for tile template dimensions
	private float roomWidth = 50.0f;
	private float roomHeight = 50.0f;

	//create array to hold different tiles
	public GameObject[] gridPrefabs;

	//create array to store grid after it's generated
	private Room [,] grid;



	// Runs before any Start() functions run
	void Awake () {
		// check to see if instance already exists
		if (instance == null) {
			// store instance in variable if none exist
			instance = this;
		} else {
			Debug.LogError ("ERROR: There can only be one GameManager.");
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () {
		//set up switch to handle choice for map generator
		switch (map) {
		case MapType.RandomLevel: 
			mapSeed = DateToInt (DateTime.Now);
			break;
		case MapType.MapOfTheDay:
			mapSeed = DateToInt (DateTime.Now.Date);
			break;
		case MapType.PresetSeed:
			mapSeed = 0;
			break;
		}


		//generate grid
		GenerateGrid ();
		//spawn enemy tanks
		SpawnEnemies ();
		//spawn player(s)
		SpawnPlayer ();

	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		


	/*  ================================================================================
	 * Map Generator functions
	 * =================================================================================
	 */

	//function to return random room/tile
	public GameObject RandomRoomPrefab () {
		return gridPrefabs [UnityEngine.Random.Range (0, gridPrefabs.Length)];
	}

	//function to return current date/time object as an int
	public int DateToInt (DateTime dateToUse) {
		//add the date up and return the result
		return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour +
			dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
	}

	public void GenerateGrid () {
		//set random seed
		UnityEngine.Random.InitState (mapSeed);

		//clear grid - "which column" is our X, "which row" is our Y
		grid = new Room[cols,rows];

		//for each grid row
		for (int i = 0; i < rows; i++) {
			//for each column in that row
			for (int j = 0; j < cols; j++) {
				//figure out the location
				float xPosition = roomWidth * j;
				float zPosition = roomHeight * i;
				Vector3 newPosition = new Vector3 (xPosition, 0.0f, zPosition);

				//create a new grid at the appropriate location
				GameObject tempRoomObj = Instantiate (RandomRoomPrefab(), newPosition, 
					Quaternion.identity) as GameObject;

				//set its parent
				tempRoomObj.transform.parent = this.transform;

				//give it a meaningful name
				tempRoomObj.name = "Room " +j+ "," +i;

				//get the room object
				Room tempRoom = tempRoomObj.GetComponent<Room> ();

				//open the inner doors
				//if in the bottom row, open the north door
				if (i == 0) {
					tempRoom.doorNorth.SetActive (false);
				} //if top row, open the south door
				else if (i == rows - 1) {
					tempRoom.doorSouth.SetActive (false);
				} //otherwise we are in the middle, open both doors
				else {
					tempRoom.doorNorth.SetActive (false);
					tempRoom.doorSouth.SetActive (false);
				}
				//if we are in the first column, open the east door
				if (j == 0) {
					tempRoom.doorEast.SetActive (false);
				} //if in last column, open west door
				else if (j == cols - 1) {
					tempRoom.doorWest.SetActive (false);
				} //otherwise we are in the middle, open both doors
				else {
					tempRoom.doorEast.SetActive (false);
					tempRoom.doorWest.SetActive (false);
				}

				//save it to the grid array
				grid [j, i] = tempRoom;


			}
		}
	}

	//========================================================================

	//Tank Spawning functions

	//spawn enemy tanks in random rooms
	public void SpawnEnemies () {
		GameObject tempTank;
		MakeList ();

		//spawn one of each of the enemy tanks in a random room at a random waypoint
		foreach (GameObject enemy in enemyTanks) {
			
			//create random integer to represent waypoint array index
			int index = RandomIndex();
			//instantiate each enemy tank in array in random room at random waypoint
			tempTank = Instantiate (enemy, enemySpawnPoints [index]) as GameObject;
		}
			
	}



	void MakeList() { //make list of all spawnpoints
		SpawnIndex = new List<int> ();
		for (int i = 0; i < enemySpawnPoints.Length; i++) {
			SpawnIndex.Add (i);
		}
	}

	int RandomIndex () { //return random int representing index that hasn't been used yet
		//return -1 if no list made
		if (SpawnIndex == null)
			return -1;
		//return -1 if list is empty
		if (SpawnIndex.Count <= 0)
			return -1;
		//return random index for spawnpoint and remove it so it is not picked again
		int spawnPtIndex = UnityEngine.Random.Range (0, SpawnIndex.Count);
		int pickedIndex = SpawnIndex [spawnPtIndex];
		SpawnIndex.RemoveAt (spawnPtIndex);
		return pickedIndex;
	}

	void SpawnPlayer () {
		GameObject playerOne;
		playerOne = Instantiate (playerPrefab, transform.position, Quaternion.identity) as GameObject;

	}

	
}
