﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shell : MonoBehaviour {

	//access Shooter script
	public TankData shooter;

	public GameObject cannonShell;


	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Enemy") {
			Debug.Log ("Hit by " + shooter.gameObject.name);
		}
	}
}
