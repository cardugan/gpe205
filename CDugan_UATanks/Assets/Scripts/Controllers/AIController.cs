﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour {

	//create enum to choose AI state
	public enum AIState {Chase, ChaseAndFire, CheckForFlee, Flee, Rest};
	public AIState aiState = AIState.Chase;
	//variable to hold time since current state was entered
	public float stateEnterTime;
	//variable to hold radius around AI to "hear" player
	public float aiSenseRadius;
	//rate at which AI recharges health when at rest
	public float restingHealRate; //in HP per second

	//vision sense of AI: variable to hold field of vision
	public float fieldOfView = 45.0f;
	public float viewDistance = 10.0f;

	//create Transform variable for target
	public Transform target;
	//create variable for transform to increase framerate
	private Transform tf;
	//access TankDate script
	private TankData data;
	//access TankMotor script
	private TankMotor motor;
	//create variable for access to Shooter script
	private Shooter shoot;

	//create timer to track time before next allowed shot
	private float lastShootTime = 0;


	//variable to hold which step of avoidance algorithm we are in
	//0 is normal movement state
	private int avoidanceStage = 0;
	//how long AI stays in this state (adjustable)
	public float avoidanceTime = 2.0f;
	//variable to track how long until AI exits state
	private float exitTime;
	//create variable for distance for AI to flee
	public float fleeDistance = 1.0f;


	//runs before Start ()
	public void Awake () {
		tf = gameObject.GetComponent<Transform> ();
	}

	// Use this for initialization
	void Start () {
		data = GetComponent <TankData> ();
		motor = GetComponent <TankMotor> ();
		shoot = GetComponent <Shooter> ();
	}
	
	// Update is called once per frame
	void Update () {

		//AIState Chase
		if (aiState == AIState.Chase) {
			//perform behaviors
			if (avoidanceStage != 0) {
				DoAvoidance ();
			} else {
				DoChase ();
			}

			//check for transitions
			//if health gets low, try to flee
			if (data.health < data.maxHealth * 0.5f) {
				ChangeState (AIState.CheckForFlee);
			//if close enough to player to sense, chase and shoot
			} else if (Vector3.SqrMagnitude (target.position - tf.position) <= (aiSenseRadius * aiSenseRadius)) {
				ChangeState (AIState.ChaseAndFire);
			}

		}//end if AIState.Chase

		else if (aiState == AIState.ChaseAndFire) {
			//perform behaviors
			if (avoidanceStage != 0) {
				DoAvoidance ();
			} else {
				DoChase ();
			}

			//limit firing rate and shoot if enough time has passed
			if (Time.time > lastShootTime + data.fireDelay) {
				shoot.ShootCannon (data.shotForce, data.shellDestruct);
				//set delay on timer
				lastShootTime = Time.time;
			}

			//check for transitions
			//if health gets low, try to flee
			if (data.health < data.maxHealth * 0.5f) {
				ChangeState (AIState.CheckForFlee);
				//if get to far away from player, just chase
			} else if (Vector3.SqrMagnitude (target.position - tf.position) > (aiSenseRadius * aiSenseRadius)) {
				ChangeState (AIState.Chase);
			}

		}//end if AIState.ChaseAndFire

		else if (aiState == AIState.Flee) {
			//perform behaviors
			if (avoidanceStage != 0) {
				DoAvoidance ();
			} else {
				DoFlee ();
			}

			//check for transitions
			//if in AIState.Flee for long enough, change to .CheckForFlee
			if (Time.time >= stateEnterTime + 30) {
				ChangeState (AIState.CheckForFlee);
			}

		}//end if AIState.Flee

		else if (aiState == AIState.CheckForFlee) {
			//perform behaviors
			CheckForFlee ();

			//check for transitions
			//if player is too close, flee
			if (Vector3.Distance (target.position, tf.position) <= aiSenseRadius) {
				ChangeState (AIState.Flee);
				//if player is far enough away, rest to recharge
			} else {
				ChangeState (AIState.Rest);
			}
		}//end if AIState.CheckForFlee

		else if (aiState == AIState.Rest) {
			//perform behaviors
			DoRest ();

			//check for transitions
			//if player gets too close, flee
			if (Vector3.SqrMagnitude (target.position - tf.position) <= (aiSenseRadius * aiSenseRadius)) {
				ChangeState (AIState.Flee);
				//if health is fully charged, go back to the chase
			} else if (data.health >= data.maxHealth) {
				ChangeState (AIState.Chase);
			}

		}//end if AIState.Rest

	}//end Update ()

	public void DoChase () {
		//turn towards target to chase
		motor.RotateTowards (target.position, data.turnSpeed);
		//check if we can move "data.moveSpeed" units away 
		//this would be how far we could move in one second
		if (CanMove (data.moveSpeed)) {
			motor.Move (data.moveSpeed);
		} else {
			//enter avoidance stage 1
			avoidanceStage = 1;
		}	
	}

	public void CheckForFlee () {
		//TODO: Write function
	}

	public void DoFlee () {
		//find vector from position to target
		Vector3 vectorToTarget = target.position - tf.position;
		//flip vector to opposite direction to get away from target
		Vector3 vectorAwayFromTarget = -vectorToTarget;
		//normalize vector to give it a magnitude of 1
		vectorAwayFromTarget.Normalize ();

		//normalized vector can be multiplied by a length to make
		// a vector of that length
		vectorAwayFromTarget *= fleeDistance;

		//find new position to flee to
		Vector3 fleePosition = vectorAwayFromTarget + tf.position;
		//rotate and move there
		motor.RotateTowards (fleePosition, data.turnSpeed);
		if (CanMove (data.moveSpeed)) {
			motor.Move (data.moveSpeed);
		} else {
			//enter avoidance stage 1
			avoidanceStage = 1;
		}	
	}

	public void DoRest () {
		//increase health per second of rest time
		data.health += restingHealRate * Time.deltaTime;
		//do not go over maxHealth
		data.health = Mathf.Min (data.health, data.maxHealth);
	}

	public void ChangeState (AIState newState) {
		//change state
		aiState = newState;

		//save the time we changed states
		stateEnterTime = Time.time;
	}

	void DoAvoidance () {
		//handles obstacle avoidance
		if (avoidanceStage == 1) {
			//rotate left
			motor.Rotate (-data.turnSpeed);
			//if able to move forward, move to stage 2
			if (CanMove (data.moveSpeed)) {
				avoidanceStage = 2;
				//set number of seconds we will stay in stage 2
				exitTime = avoidanceTime;
			}
			//otherwise we'll do this again next turn
		}else if (avoidanceStage == 2) {
			//move forward if able
			if (CanMove (data.moveSpeed)) {
				//subtract from timer and move
				exitTime -= Time.deltaTime;
				motor.Move (data.moveSpeed);
				//if timer runs out, return to normal mode
				if (exitTime <= 0) {
					avoidanceStage = 0;
				}
			} else {
				//if can't move forward, go back to stage 1
				avoidanceStage = 1;
			}
		}

	}


	bool CanMove (float speed) {
		//cast ray forward to see if it hits anything
		RaycastHit hit;
		//if it hits something
		if (Physics.Raycast (tf.position, tf.forward, out hit, speed)) {
			//and if what we hit is not the player
			if (!hit.collider.CompareTag ("Player")) {
				//then we can't move
				return false;
			}
		}
		//otherwise we can move
		return true;
	}

	public bool CanSee (GameObject target) {

		//store location of target in variable
		Vector3 targetPosition = target.transform.position;

		//find vector form AI to player
		Vector3 aiToTargetVector = targetPosition - tf.position;

		//find the angle between the direction our agent is facing and the vector to the target
		float angleToTarget = Vector3.Angle (aiToTargetVector, tf.forward);

		//if the angle is less than our FOV 
		if (angleToTarget < fieldOfView) {
			//create variable to how ray from ai to target
			Ray rayToTarget = new Ray ();
			//set origin and direction of ray
			rayToTarget.origin = tf.position;
			rayToTarget.direction = aiToTargetVector;

			//create variable to hold info about what ray hits
			RaycastHit hit;

			//cast ray and see if it hits something
			if (Physics.Raycast (rayToTarget, out hit, viewDistance)) {
				//if it hits the target
				if (hit.collider.gameObject == target) {
					//canSee is true
					return true;
				}
			}
		}
			//canSee is false
			return false;
		}

}
