﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheatController : MonoBehaviour {

	//variable to hold PowerupController
	public PowerupController powCon;
	//variable to hold data of powerup
	public Powerup cheatPowerup;


	// Use this for initialization
	void Start () {
		if (powCon == null) {
			powCon = gameObject.GetComponent <PowerupController> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		//if H and U are down and this is the first frame that E is pressed
		if (Input.GetKey (KeyCode.H) && Input.GetKey (KeyCode.U) && Input.GetKeyDown (KeyCode.E)) {
			//add powerup to tank
			powCon.Add (cheatPowerup);
		}
		
	}
}
